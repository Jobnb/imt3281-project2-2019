package no.ntnu.imt3281.ludo.logic;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import java.lang.String;
import javafx.scene.Parent;



public class Ludo extends Application {

    private static final ResourceBundle messages = ResourceBundle.getBundle("i18n.Messages", Locale.getDefault());
    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        stage.setTitle(messages.getString("title"));
        scene = new Scene(loadFXML("../gui/GameBoard"));
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Ludo.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }


    public static void main(String[] args) {
        Ludo ludo = new Ludo();
        launch(args);
    }
}