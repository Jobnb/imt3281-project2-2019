package no.ntnu.imt3281.ludo.logic;

public class Board {
    Pieces pieces;
    StartArea startArea;
    PlayArea playArea;
    WinArea winArea;

    Board() {
        Pieces pieces = new Pieces();
        StartArea startArea = new StartArea();
        PlayArea playArea = new PlayArea();
        WinArea winArea = new WinArea();
    }
}
